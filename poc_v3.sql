-- Volcando estructura para tabla poc.booking_line
CREATE TABLE IF NOT EXISTS `booking` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `price_cancelled` decimal(19,2),
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando estructura para tabla poc.booking_line_providers
CREATE TABLE IF NOT EXISTS `booking_line_bookings` (
  `bookinglines_id` bigint(20) NOT NULL,
  `bookings_id` bigint(20) NOT NULL,
  PRIMARY KEY (`bookinglines_id`,`bookings_id`),
  KEY `FKrvo8sxpkx6bk9wwdf7ij5taqw` (`bookinglines_id`),
  CONSTRAINT `FKffndk6xevgxaaq6ooj59aumae` FOREIGN KEY (`bookinglines_id`) REFERENCES `booking_line` (`id`),
  CONSTRAINT `FKrvo8sxpkx6bk9ww7f7ij7taqw` FOREIGN KEY (`bookings_id`) REFERENCES `booking` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;