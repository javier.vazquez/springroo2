roo

// Spring Roo 2.0.0.RC1 [rev 28015e3] log opened at 2017-06-23 12:46:13
project setup --topLevelPackage com.springsource.poc --projectName poc_roo_2_0
jpa setup --provider HIBERNATE --database MYSQL --userName root --databaseName poc --hostName 10.1.19.223
//jpa setup --provider HIBERNATE --database MYSQL
enum type --class ~.reference.StatusType
enum constant --name Opened
enum constant --name Cancelled
enum constant --name Closed

entity jpa --class ~.domain.Provider --entityFormatExpression "#{description}" --sequenceName PROVIDER_SEQ
entity jpa --class ~.domain.BookingLine --entityFormatExpression "#{description} #{priceCost}" --sequenceName BOOKINGLINE_SEQ
entity jpa --class ~.domain.ContractClientProvider --entityFormatExpression "#{description}" --sequenceName CONTRACTCLIENTPROVIDER_SEQ


focus --class ~.domain.BookingLine
field string --fieldName description --sizeMin 1 --sizeMax 255 --notNull
field boolean --fieldName cancelled
field date --fieldName fromDate --type java.util.Date --notNull --past
field date --fieldName toDate --type java.util.Date --notNull --past
field number --fieldName priceCost --type java.math.BigDecimal --notNull --min 0
field number --fieldName priceSell --type java.math.BigDecimal --notNull --min 0
field enum --fieldName status --type ~.reference.StatusType --notNull
//field reference --fieldName provider --type ~.domain.Provider --notNull
//field reference --fieldName contractClientProvider --type ~.domain.ContractClientProvider --notNull

focus --class ~.domain.Provider
field string --fieldName description  --notNull  --sizeMin 1
field set --fieldName bookingLines --type ~.domain.BookingLine --cardinality ONE_TO_MANY --mappedBy provider
field set --fieldName contractClientProviders --type ~.domain.ContractClientProvider --cardinality ONE_TO_MANY --mappedBy provider

focus --class ~.domain.ContractClientProvider
field string --fieldName description  --notNull  --sizeMin 1
//field reference --fieldName provider --type ~.domain.Provider
field set --fieldName bookingLines --type ~.domain.BookingLine --mappedBy contractClientProvider --notNull false --cardinality ONE_TO_MANY