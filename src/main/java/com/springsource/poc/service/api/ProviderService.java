package com.springsource.poc.service.api;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.layers.service.annotations.RooService;

/**
 * = ProviderService
 TODO Auto-generated class documentation
 *
 */
@RooService(entity = Provider.class)
public interface ProviderService {
}
