package com.springsource.poc.service.api;
import com.springsource.poc.domain.BookingLine;
import org.springframework.roo.addon.layers.service.annotations.RooService;

/**
 * = BookingLineService
 TODO Auto-generated class documentation
 *
 */
@RooService(entity = BookingLine.class)
public interface BookingLineService {
}
