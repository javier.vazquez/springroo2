package com.springsource.poc.domain.dto;
import org.springframework.roo.addon.dto.annotations.RooDTO;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import java.math.BigDecimal;
import org.springframework.format.annotation.NumberFormat;

/**
 * = BookinglineDescriptionAndCancelledAndPriceCostAndPriceSellFormBean
 TODO Auto-generated class documentation
 *
 */
@RooDTO
@RooJavaBean
public class BookinglineDescriptionAndCancelledAndPriceCostAndPriceSellFormBean {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private String description;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private Boolean cancelled;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NumberFormat
    private BigDecimal priceCost;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NumberFormat
    private BigDecimal priceSell;
}
