package com.springsource.poc.domain.dto;
import org.springframework.roo.addon.dto.annotations.RooDTO;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;

/**
 * = ProviderDescriptionFormBean
 TODO Auto-generated class documentation
 *
 */
@RooDTO
@RooJavaBean
public class ProviderDescriptionFormBean {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private String description;
}
