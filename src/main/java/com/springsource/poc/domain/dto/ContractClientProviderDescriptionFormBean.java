package com.springsource.poc.domain.dto;
import org.springframework.roo.addon.dto.annotations.RooDTO;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;

/**
 * = ContractClientProviderDescriptionFormBean
 TODO Auto-generated class documentation
 *
 */
@RooDTO
@RooJavaBean
public class ContractClientProviderDescriptionFormBean {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private String description;
}
