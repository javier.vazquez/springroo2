package com.springsource.poc.repository;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;

/**
 * = BookingRepositoryCustom
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = Booking.class)
public interface BookingRepositoryCustom {
}
