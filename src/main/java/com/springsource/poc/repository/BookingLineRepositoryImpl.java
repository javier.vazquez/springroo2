package com.springsource.poc.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.springsource.poc.domain.BookingLine;

/**
 * = BookingLineRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */ 
@RooJpaRepositoryCustomImpl(repository = BookingLineRepositoryCustom.class)
public class BookingLineRepositoryImpl extends QueryDslRepositorySupportExt<BookingLine> {

    /**
     * TODO Auto-generated constructor documentation
     */
    BookingLineRepositoryImpl() {
        super(BookingLine.class);
    }
}