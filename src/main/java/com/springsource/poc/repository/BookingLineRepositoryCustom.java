package com.springsource.poc.repository;
import com.springsource.poc.domain.BookingLine;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;

/**
 * = BookingLineRepositoryCustom
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = BookingLine.class)
public interface BookingLineRepositoryCustom {
}
