package com.springsource.poc.repository;
import com.springsource.poc.domain.ContractClientProvider;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;

/**
 * = ContractClientProviderRepositoryCustom
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = ContractClientProvider.class)
public interface ContractClientProviderRepositoryCustom {
}
