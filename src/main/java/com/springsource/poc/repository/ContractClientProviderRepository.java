package com.springsource.poc.repository;
import com.springsource.poc.domain.ContractClientProvider;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import org.springframework.roo.addon.layers.repository.jpa.annotations.finder.RooFinder;
import com.springsource.poc.domain.dto.ContractClientProviderDescriptionFormBean;

/**
 * = ContractClientProviderRepository
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = ContractClientProvider.class, finders = { @RooFinder(value = "findByDescriptionLike", returnType = ContractClientProvider.class), @RooFinder(value = "findByDescription", returnType = ContractClientProvider.class, formBean = ContractClientProviderDescriptionFormBean.class) })
public interface ContractClientProviderRepository {
}
