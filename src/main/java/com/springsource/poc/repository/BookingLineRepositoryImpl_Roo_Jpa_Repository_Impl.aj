// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import com.springsource.poc.domain.Booking;
import com.springsource.poc.domain.BookingLine;
import com.springsource.poc.domain.ContractClientProvider;
import com.springsource.poc.domain.Provider;
import com.springsource.poc.domain.QBookingLine;
import com.springsource.poc.domain.dto.BookinglineDescriptionAndCancelledAndPriceCostAndPriceSellFormBean;
import com.springsource.poc.repository.BookingLineRepositoryCustom;
import com.springsource.poc.repository.BookingLineRepositoryImpl;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

privileged aspect BookingLineRepositoryImpl_Roo_Jpa_Repository_Impl {
    
    declare parents: BookingLineRepositoryImpl implements BookingLineRepositoryCustom;
    
    declare @type: BookingLineRepositoryImpl: @Transactional(readOnly = true);
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.DESCRIPTION = "description";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.CANCELLED = "cancelled";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.FROM_DATE = "fromDate";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.TO_DATE = "toDate";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.PRICE_COST = "priceCost";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.PRICE_SELL = "priceSell";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.STATUS = "status";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String BookingLineRepositoryImpl.CONTRACT_CLIENT_PROVIDER = "contractClientProvider";
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<BookingLine> BookingLineRepositoryImpl.findAll(GlobalSearch globalSearch, Pageable pageable) {
        
        QBookingLine bookingLine = QBookingLine.bookingLine;
        
        JPQLQuery<BookingLine> query = from(bookingLine);
        
        Path<?>[] paths = new Path<?>[] {bookingLine.description,bookingLine.cancelled,bookingLine.fromDate,bookingLine.toDate,bookingLine.priceCost,bookingLine.priceSell,bookingLine.status,bookingLine.contractClientProvider};        
        applyGlobalSearch(globalSearch, query, paths);
        
        AttributeMappingBuilder mapping = buildMapper()
			.map(DESCRIPTION, bookingLine.description)
			.map(CANCELLED, bookingLine.cancelled)
			.map(FROM_DATE, bookingLine.fromDate)
			.map(TO_DATE, bookingLine.toDate)
			.map(PRICE_COST, bookingLine.priceCost)
			.map(PRICE_SELL, bookingLine.priceSell)
			.map(STATUS, bookingLine.status)
			.map(CONTRACT_CLIENT_PROVIDER, bookingLine.contractClientProvider);
        
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        
        return loadPage(query, pageable, bookingLine);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookings
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<BookingLine> BookingLineRepositoryImpl.findByBookingsContains(Booking bookings, GlobalSearch globalSearch, Pageable pageable) {
        
        QBookingLine bookingLine = QBookingLine.bookingLine;
        
        JPQLQuery<BookingLine> query = from(bookingLine);
        
        Assert.notNull(bookings, "bookings is required");
        
        query.where(bookingLine.bookings.contains(bookings));
        Path<?>[] paths = new Path<?>[] {bookingLine.description,bookingLine.cancelled,bookingLine.fromDate,bookingLine.toDate,bookingLine.priceCost,bookingLine.priceSell,bookingLine.status,bookingLine.contractClientProvider};        
        applyGlobalSearch(globalSearch, query, paths);
        
        AttributeMappingBuilder mapping = buildMapper()
			.map(DESCRIPTION, bookingLine.description)
			.map(CANCELLED, bookingLine.cancelled)
			.map(FROM_DATE, bookingLine.fromDate)
			.map(TO_DATE, bookingLine.toDate)
			.map(PRICE_COST, bookingLine.priceCost)
			.map(PRICE_SELL, bookingLine.priceSell)
			.map(STATUS, bookingLine.status)
			.map(CONTRACT_CLIENT_PROVIDER, bookingLine.contractClientProvider);
        
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        
        return loadPage(query, pageable, bookingLine);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProvider
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<BookingLine> BookingLineRepositoryImpl.findByContractClientProvider(ContractClientProvider contractClientProvider, GlobalSearch globalSearch, Pageable pageable) {
        
        QBookingLine bookingLine = QBookingLine.bookingLine;
        
        JPQLQuery<BookingLine> query = from(bookingLine);
        
        Assert.notNull(contractClientProvider, "contractClientProvider is required");
        
        query.where(bookingLine.contractClientProvider.eq(contractClientProvider));
        Path<?>[] paths = new Path<?>[] {bookingLine.description,bookingLine.cancelled,bookingLine.fromDate,bookingLine.toDate,bookingLine.priceCost,bookingLine.priceSell,bookingLine.status,bookingLine.contractClientProvider};        
        applyGlobalSearch(globalSearch, query, paths);
        
        AttributeMappingBuilder mapping = buildMapper()
			.map(DESCRIPTION, bookingLine.description)
			.map(CANCELLED, bookingLine.cancelled)
			.map(FROM_DATE, bookingLine.fromDate)
			.map(TO_DATE, bookingLine.toDate)
			.map(PRICE_COST, bookingLine.priceCost)
			.map(PRICE_SELL, bookingLine.priceSell)
			.map(STATUS, bookingLine.status)
			.map(CONTRACT_CLIENT_PROVIDER, bookingLine.contractClientProvider);
        
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        
        return loadPage(query, pageable, bookingLine);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param providers
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<BookingLine> BookingLineRepositoryImpl.findByProvidersContains(Provider providers, GlobalSearch globalSearch, Pageable pageable) {
        
        QBookingLine bookingLine = QBookingLine.bookingLine;
        
        JPQLQuery<BookingLine> query = from(bookingLine);
        
        Assert.notNull(providers, "providers is required");
        
        query.where(bookingLine.providers.contains(providers));
        Path<?>[] paths = new Path<?>[] {bookingLine.description,bookingLine.cancelled,bookingLine.fromDate,bookingLine.toDate,bookingLine.priceCost,bookingLine.priceSell,bookingLine.status,bookingLine.contractClientProvider};        
        applyGlobalSearch(globalSearch, query, paths);
        
        AttributeMappingBuilder mapping = buildMapper()
			.map(DESCRIPTION, bookingLine.description)
			.map(CANCELLED, bookingLine.cancelled)
			.map(FROM_DATE, bookingLine.fromDate)
			.map(TO_DATE, bookingLine.toDate)
			.map(PRICE_COST, bookingLine.priceCost)
			.map(PRICE_SELL, bookingLine.priceSell)
			.map(STATUS, bookingLine.status)
			.map(CONTRACT_CLIENT_PROVIDER, bookingLine.contractClientProvider);
        
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        
        return loadPage(query, pageable, bookingLine);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param formBean
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<BookingLine> BookingLineRepositoryImpl.findByDescriptionAndCancelledAndPriceCostAndPriceSell(BookinglineDescriptionAndCancelledAndPriceCostAndPriceSellFormBean formBean, GlobalSearch globalSearch, Pageable pageable) {
        
        QBookingLine bookingLine = QBookingLine.bookingLine;
        
        JPQLQuery<BookingLine> query = from(bookingLine);
        
        if (formBean != null) {
        BooleanBuilder searchCondition = new BooleanBuilder();
                if (formBean.getDescription() != null) {
                        searchCondition.and(bookingLine.description.eq(formBean.getDescription()));
                }
                if (formBean.getCancelled() != null) {
                        searchCondition.and(bookingLine.cancelled.eq(formBean.getCancelled()));
                }
                if (formBean.getPriceCost() != null) {
                        searchCondition.and(bookingLine.priceCost.eq(formBean.getPriceCost()));
                }
                if (formBean.getPriceSell() != null) {
                        searchCondition.and(bookingLine.priceSell.eq(formBean.getPriceSell()));
                }
        if (searchCondition.hasValue()) {
            query.where(searchCondition);
        }
        }
        
        Path<?>[] paths = new Path<?>[] {bookingLine.description,bookingLine.cancelled,bookingLine.fromDate,bookingLine.toDate,bookingLine.priceCost,bookingLine.priceSell,bookingLine.status,bookingLine.contractClientProvider};        
        applyGlobalSearch(globalSearch, query, paths);
        
        AttributeMappingBuilder mapping = buildMapper()
			.map(DESCRIPTION, bookingLine.description)
			.map(CANCELLED, bookingLine.cancelled)
			.map(FROM_DATE, bookingLine.fromDate)
			.map(TO_DATE, bookingLine.toDate)
			.map(PRICE_COST, bookingLine.priceCost)
			.map(PRICE_SELL, bookingLine.priceSell)
			.map(STATUS, bookingLine.status)
			.map(CONTRACT_CLIENT_PROVIDER, bookingLine.contractClientProvider);
        
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        
        return loadPage(query, pageable, bookingLine);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param formBean
     * @return Long
     */
    public long BookingLineRepositoryImpl.countByDescriptionAndCancelledAndPriceCostAndPriceSell(BookinglineDescriptionAndCancelledAndPriceCostAndPriceSellFormBean formBean) {
        
        QBookingLine bookingLine = QBookingLine.bookingLine;
        
        JPQLQuery<BookingLine> query = from(bookingLine);
        
        BooleanBuilder searchCondition = new BooleanBuilder();
                if (formBean.getDescription() != null) {
                        searchCondition.and(bookingLine.description.eq(formBean.getDescription()));
                }
                if (formBean.getCancelled() != null) {
                        searchCondition.and(bookingLine.cancelled.eq(formBean.getCancelled()));
                }
                if (formBean.getPriceCost() != null) {
                        searchCondition.and(bookingLine.priceCost.eq(formBean.getPriceCost()));
                }
                if (formBean.getPriceSell() != null) {
                        searchCondition.and(bookingLine.priceSell.eq(formBean.getPriceSell()));
                }
        if (searchCondition.hasValue()) {
            query.where(searchCondition);
        }
        return query.fetchCount();
    }
    
}
