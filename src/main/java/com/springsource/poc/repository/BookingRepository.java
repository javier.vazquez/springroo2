package com.springsource.poc.repository;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;

/**
 * = BookingRepository
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Booking.class)
public interface BookingRepository {
}
