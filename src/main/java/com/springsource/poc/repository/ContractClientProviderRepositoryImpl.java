package com.springsource.poc.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.springsource.poc.domain.ContractClientProvider;

/**
 * = ContractClientProviderRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */ 
@RooJpaRepositoryCustomImpl(repository = ContractClientProviderRepositoryCustom.class)
public class ContractClientProviderRepositoryImpl extends QueryDslRepositorySupportExt<ContractClientProvider> {

    /**
     * TODO Auto-generated constructor documentation
     */
    ContractClientProviderRepositoryImpl() {
        super(ContractClientProvider.class);
    }
}