package com.springsource.poc.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.springsource.poc.domain.Booking;

/**
 * = BookingRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */ 
@RooJpaRepositoryCustomImpl(repository = BookingRepositoryCustom.class)
public class BookingRepositoryImpl extends QueryDslRepositorySupportExt<Booking> {

    /**
     * TODO Auto-generated constructor documentation
     */
    BookingRepositoryImpl() {
        super(Booking.class);
    }
}