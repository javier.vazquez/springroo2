package com.springsource.poc.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.springsource.poc.domain.Provider;

/**
 * = ProviderRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */ 
@RooJpaRepositoryCustomImpl(repository = ProviderRepositoryCustom.class)
public class ProviderRepositoryImpl extends QueryDslRepositorySupportExt<Provider> {

    /**
     * TODO Auto-generated constructor documentation
     */
    ProviderRepositoryImpl() {
        super(Provider.class);
    }
}