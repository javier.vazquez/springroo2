package com.springsource.poc.repository;
import com.springsource.poc.domain.BookingLine;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import org.springframework.roo.addon.layers.repository.jpa.annotations.finder.RooFinder;
import com.springsource.poc.domain.dto.BookinglineDescriptionAndCancelledAndPriceCostAndPriceSellFormBean;

/**
 * = BookingLineRepository
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = BookingLine.class, finders = { @RooFinder(value = "findByDescriptionLike", returnType = BookingLine.class), @RooFinder(value = "findByDescriptionLikeAndCancelledAndFromDateBetweenAndToDateBetweenAndPriceCostAndContractClientProvider", returnType = BookingLine.class), @RooFinder(value = "findByDescriptionAndCancelledAndPriceCostAndPriceSell", returnType = BookingLine.class, formBean = BookinglineDescriptionAndCancelledAndPriceCostAndPriceSellFormBean.class) })
public interface BookingLineRepository {
}
