// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.repository;

import com.springsource.poc.domain.ContractClientProvider;
import com.springsource.poc.domain.Provider;
import com.springsource.poc.repository.ContractClientProviderRepository;
import com.springsource.poc.repository.ContractClientProviderRepositoryCustom;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

privileged aspect ContractClientProviderRepository_Roo_Jpa_Repository {
    
    declare parents: ContractClientProviderRepository extends DetachableJpaRepository<ContractClientProvider, Long>;
    
    declare parents: ContractClientProviderRepository extends ContractClientProviderRepositoryCustom;
    
    declare @type: ContractClientProviderRepository: @Transactional(readOnly = true);
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param providers
     * @return Long
     */
    public abstract long ContractClientProviderRepository.countByProvidersContains(Provider providers);
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param description
     * @param pageable
     * @return Page
     */
    public abstract Page<ContractClientProvider> ContractClientProviderRepository.findByDescriptionLike(String description, Pageable pageable);
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param description
     * @return Long
     */
    public abstract long ContractClientProviderRepository.countByDescriptionLike(String description);
    
}
