package com.springsource.poc.repository;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;

/**
 * = ProviderRepositoryCustom
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = Provider.class)
public interface ProviderRepositoryCustom {
}
