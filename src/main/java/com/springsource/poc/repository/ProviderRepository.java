package com.springsource.poc.repository;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import org.springframework.roo.addon.layers.repository.jpa.annotations.finder.RooFinder;
import com.springsource.poc.domain.dto.ProviderDescriptionFormBean;

/**
 * = ProviderRepository
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Provider.class, finders = { @RooFinder(value = "findByDescriptionLike", returnType = Provider.class), @RooFinder(value = "findByDescription", returnType = Provider.class, formBean = ProviderDescriptionFormBean.class) })
public interface ProviderRepository {
}
