package com.springsource.poc.web;
import com.springsource.poc.domain.BookingLine;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = BookingLinesItemJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = BookingLine.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
public class BookingLinesItemJsonController {
}
