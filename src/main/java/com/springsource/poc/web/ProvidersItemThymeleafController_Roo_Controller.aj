// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.service.api.ProviderService;
import com.springsource.poc.web.ProvidersItemThymeleafController;

privileged aspect ProvidersItemThymeleafController_Roo_Controller {
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private ProviderService ProvidersItemThymeleafController.providerService;
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ProviderService
     */
    public ProviderService ProvidersItemThymeleafController.getProviderService() {
        return providerService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param providerService
     */
    public void ProvidersItemThymeleafController.setProviderService(ProviderService providerService) {
        this.providerService = providerService;
    }
    
}
