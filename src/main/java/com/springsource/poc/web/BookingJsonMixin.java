package com.springsource.poc.web;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;

/**
 * = BookingJsonMixin
 TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Booking.class)
public abstract class BookingJsonMixin {
}
