package com.springsource.poc.web;
import com.springsource.poc.domain.ContractClientProvider;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;

/**
 * = ContractClientProviderJsonMixin
 TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = ContractClientProvider.class)
public abstract class ContractClientProviderJsonMixin {
}
