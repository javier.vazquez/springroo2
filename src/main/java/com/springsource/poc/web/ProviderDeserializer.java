package com.springsource.poc.web;
import com.springsource.poc.domain.Provider;
import com.springsource.poc.service.api.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;

/**
 * = ProviderDeserializer
 TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Provider.class)
public class ProviderDeserializer extends JsonObjectDeserializer<Provider> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ProviderService providerService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param providerService
     * @param conversionService
     */
    @Autowired
    public ProviderDeserializer(@Lazy ProviderService providerService, ConversionService conversionService) {
        this.providerService = providerService;
        this.conversionService = conversionService;
    }
}
