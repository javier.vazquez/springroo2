package com.springsource.poc.web;
import com.springsource.poc.domain.BookingLine;
import com.springsource.poc.service.api.BookingLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;

/**
 * = BookingLineDeserializer
 TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = BookingLine.class)
public class BookingLineDeserializer extends JsonObjectDeserializer<BookingLine> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private BookingLineService bookingLineService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param bookingLineService
     * @param conversionService
     */
    @Autowired
    public BookingLineDeserializer(@Lazy BookingLineService bookingLineService, ConversionService conversionService) {
        this.bookingLineService = bookingLineService;
        this.conversionService = conversionService;
    }
}
