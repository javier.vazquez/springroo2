package com.springsource.poc.web;
import com.springsource.poc.domain.BookingLine;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;

/**
 * = BookingLineJsonMixin
 TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = BookingLine.class)
public abstract class BookingLineJsonMixin {
}
