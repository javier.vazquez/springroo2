// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.service.api.BookingService;
import com.springsource.poc.web.BookingsCollectionThymeleafController;

privileged aspect BookingsCollectionThymeleafController_Roo_Controller {
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private BookingService BookingsCollectionThymeleafController.bookingService;
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return BookingService
     */
    public BookingService BookingsCollectionThymeleafController.getBookingService() {
        return bookingService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookingService
     */
    public void BookingsCollectionThymeleafController.setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }
    
}
