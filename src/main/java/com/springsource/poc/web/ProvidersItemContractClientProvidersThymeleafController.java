package com.springsource.poc.web;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooDetail;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = ProvidersItemContractClientProvidersThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Provider.class, type = ControllerType.DETAIL)
@RooDetail(relationField = "contractClientProviders", views = { "list", "show", "findByDescription" })
@RooThymeleaf
public class ProvidersItemContractClientProvidersThymeleafController {
}
