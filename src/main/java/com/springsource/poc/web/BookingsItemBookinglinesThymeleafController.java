package com.springsource.poc.web;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooDetail;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = BookingsItemBookinglinesThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Booking.class, type = ControllerType.DETAIL)
@RooDetail(relationField = "bookinglines", views = { "list", "show" })
@RooThymeleaf
public class BookingsItemBookinglinesThymeleafController {
}
