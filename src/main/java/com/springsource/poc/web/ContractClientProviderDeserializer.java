package com.springsource.poc.web;
import com.springsource.poc.domain.ContractClientProvider;
import com.springsource.poc.service.api.ContractClientProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;

/**
 * = ContractClientProviderDeserializer
 TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = ContractClientProvider.class)
public class ContractClientProviderDeserializer extends JsonObjectDeserializer<ContractClientProvider> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ContractClientProviderService contractClientProviderService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param contractClientProviderService
     * @param conversionService
     */
    @Autowired
    public ContractClientProviderDeserializer(@Lazy ContractClientProviderService contractClientProviderService, ConversionService conversionService) {
        this.contractClientProviderService = contractClientProviderService;
        this.conversionService = conversionService;
    }
}
