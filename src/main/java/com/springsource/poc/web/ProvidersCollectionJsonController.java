package com.springsource.poc.web;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = ProvidersCollectionJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Provider.class, pathPrefix = "/api", type = ControllerType.COLLECTION)
@RooJSON
public class ProvidersCollectionJsonController {
}
