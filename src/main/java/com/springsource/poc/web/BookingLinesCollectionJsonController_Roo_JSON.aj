// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.domain.BookingLine;
import com.springsource.poc.service.api.BookingLineService;
import com.springsource.poc.web.BookingLinesCollectionJsonController;
import com.springsource.poc.web.BookingLinesItemJsonController;
import io.springlets.data.domain.GlobalSearch;
import java.util.Collection;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

privileged aspect BookingLinesCollectionJsonController_Roo_JSON {
    
    declare @type: BookingLinesCollectionJsonController: @RestController;
    
    declare @type: BookingLinesCollectionJsonController: @RequestMapping(value = "/api/bookinglines", name = "BookingLinesCollectionJsonController", produces = MediaType.APPLICATION_JSON_VALUE);
    
    /**
     * TODO Auto-generated constructor documentation
     * 
     * @param bookingLineService
     */
    @Autowired
    public BookingLinesCollectionJsonController.new(BookingLineService bookingLineService) {
        this.bookingLineService = bookingLineService;
    }

    /**
     * TODO Auto-generated method documentation
     * 
     * @param globalSearch
     * @param pageable
     * @return ResponseEntity
     */
    @GetMapping(name = "list")
    public ResponseEntity<Page<BookingLine>> BookingLinesCollectionJsonController.list(GlobalSearch globalSearch, Pageable pageable) {
        
        Page<BookingLine> bookingLines = getBookingLineService().findAll(globalSearch, pageable);
        return ResponseEntity.ok(bookingLines);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return UriComponents
     */
    public static UriComponents BookingLinesCollectionJsonController.listURI() {
        return MvcUriComponentsBuilder
            .fromMethodCall(
                MvcUriComponentsBuilder.on(BookingLinesCollectionJsonController.class).list(null, null))
            .build().encode();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookingLine
     * @param result
     * @return ResponseEntity
     */
    @PostMapping(name = "create")
    public ResponseEntity<?> BookingLinesCollectionJsonController.create(@Valid @RequestBody BookingLine bookingLine, BindingResult result) {
        
        if (bookingLine.getId() != null || bookingLine.getVersion() != null) {        
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        
        BookingLine newBookingLine = getBookingLineService().save(bookingLine);
        UriComponents showURI = BookingLinesItemJsonController.showURI(newBookingLine);
        
        return ResponseEntity.created(showURI.toUri()).build();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookingLines
     * @param result
     * @return ResponseEntity
     */
    @PostMapping(value = "/batch", name = "createBatch")
    public ResponseEntity<?> BookingLinesCollectionJsonController.createBatch(@Valid @RequestBody Collection<BookingLine> bookingLines, BindingResult result) {
        
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        
        getBookingLineService().save(bookingLines);
        
        return ResponseEntity.created(listURI().toUri()).build();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookingLines
     * @param result
     * @return ResponseEntity
     */
    @PutMapping(value = "/batch", name = "updateBatch")
    public ResponseEntity<?> BookingLinesCollectionJsonController.updateBatch(@Valid @RequestBody Collection<BookingLine> bookingLines, BindingResult result) {
        
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        
        getBookingLineService().save(bookingLines);
        
        return ResponseEntity.ok().build();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param ids
     * @return ResponseEntity
     */
    @DeleteMapping(value = "/batch/{ids}", name = "deleteBatch")
    public ResponseEntity<?> BookingLinesCollectionJsonController.deleteBatch(@PathVariable("ids") Collection<Long> ids) {
        
        getBookingLineService().delete(ids);
        
        return ResponseEntity.ok().build();
    }
    
}
