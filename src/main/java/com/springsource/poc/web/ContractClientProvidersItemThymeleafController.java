package com.springsource.poc.web;
import com.springsource.poc.domain.ContractClientProvider;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = ContractClientProvidersItemThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = ContractClientProvider.class, type = ControllerType.ITEM)
@RooThymeleaf
public class ContractClientProvidersItemThymeleafController {
}
