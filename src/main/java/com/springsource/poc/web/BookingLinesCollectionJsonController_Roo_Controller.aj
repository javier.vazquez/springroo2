// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.service.api.BookingLineService;
import com.springsource.poc.web.BookingLinesCollectionJsonController;

privileged aspect BookingLinesCollectionJsonController_Roo_Controller {
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private BookingLineService BookingLinesCollectionJsonController.bookingLineService;
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return BookingLineService
     */
    public BookingLineService BookingLinesCollectionJsonController.getBookingLineService() {
        return bookingLineService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookingLineService
     */
    public void BookingLinesCollectionJsonController.setBookingLineService(BookingLineService bookingLineService) {
        this.bookingLineService = bookingLineService;
    }
    
}
