// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.domain.ContractClientProvider;
import com.springsource.poc.service.api.ContractClientProviderService;
import com.springsource.poc.web.ContractClientProvidersItemThymeleafController;
import com.springsource.poc.web.ContractClientProvidersItemThymeleafLinkFactory;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

privileged aspect ContractClientProvidersItemThymeleafController_Roo_Thymeleaf {
    
    declare @type: ContractClientProvidersItemThymeleafController: @Controller;
    
    declare @type: ContractClientProvidersItemThymeleafController: @RequestMapping(value = "/contractclientproviders/{contractClientProvider}", name = "ContractClientProvidersItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE);
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private MessageSource ContractClientProvidersItemThymeleafController.messageSource;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private MethodLinkBuilderFactory<ContractClientProvidersItemThymeleafController> ContractClientProvidersItemThymeleafController.itemLink;
    
    /**
     * TODO Auto-generated constructor documentation
     * 
     * @param contractClientProviderService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public ContractClientProvidersItemThymeleafController.new(ContractClientProviderService contractClientProviderService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setContractClientProviderService(contractClientProviderService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(ContractClientProvidersItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     * 
     * @return MessageSource
     */
    public MessageSource ContractClientProvidersItemThymeleafController.getMessageSource() {
        return messageSource;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param messageSource
     */
    public void ContractClientProvidersItemThymeleafController.setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<ContractClientProvidersItemThymeleafController> ContractClientProvidersItemThymeleafController.getItemLink() {
        return itemLink;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param itemLink
     */
    public void ContractClientProvidersItemThymeleafController.setItemLink(MethodLinkBuilderFactory<ContractClientProvidersItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param id
     * @param locale
     * @param method
     * @return ContractClientProvider
     */
    @ModelAttribute
    public ContractClientProvider ContractClientProvidersItemThymeleafController.getContractClientProvider(@PathVariable("contractClientProvider") Long id, Locale locale, HttpMethod method) {
        ContractClientProvider contractClientProvider = null;
        if (HttpMethod.PUT.equals(method)) {
            contractClientProvider = contractClientProviderService.findOneForUpdate(id);
        } else {
            contractClientProvider = contractClientProviderService.findOne(id);
        }
        
        if (contractClientProvider == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] {"ContractClientProvider", id}, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return contractClientProvider;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProvider
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView ContractClientProvidersItemThymeleafController.show(@ModelAttribute ContractClientProvider contractClientProvider, Model model) {
        model.addAttribute("contractClientProvider", contractClientProvider);
        return new ModelAndView("contractclientproviders/show");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProvider
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView ContractClientProvidersItemThymeleafController.showInline(@ModelAttribute ContractClientProvider contractClientProvider, Model model) {
        model.addAttribute("contractClientProvider", contractClientProvider);
        return new ModelAndView("contractclientproviders/showInline :: inline-content");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param dataBinder
     */
    @InitBinder("contractClientProvider")
    public void ContractClientProvidersItemThymeleafController.initContractClientProviderBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param model
     */
    public void ContractClientProvidersItemThymeleafController.populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param model
     */
    public void ContractClientProvidersItemThymeleafController.populateForm(Model model) {
        populateFormats(model);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProvider
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView ContractClientProvidersItemThymeleafController.editForm(@ModelAttribute ContractClientProvider contractClientProvider, Model model) {
        populateForm(model);
        
        model.addAttribute("contractClientProvider", contractClientProvider);
        return new ModelAndView("contractclientproviders/edit");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProvider
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView ContractClientProvidersItemThymeleafController.update(@Valid @ModelAttribute ContractClientProvider contractClientProvider, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            
            return new ModelAndView("contractclientproviders/edit");
        }
        // Concurrency control
        ContractClientProvider existingContractClientProvider = getContractClientProviderService().findOne(contractClientProvider.getId());
        if(contractClientProvider.getVersion() != existingContractClientProvider.getVersion() && StringUtils.isEmpty(concurrencyControl)){
            populateForm(model);
            model.addAttribute("contractClientProvider", contractClientProvider);
            model.addAttribute("concurrency", true);
            return new ModelAndView("contractclientproviders/edit");
        } else if(contractClientProvider.getVersion() != existingContractClientProvider.getVersion() && "discard".equals(concurrencyControl)){
            populateForm(model);
            model.addAttribute("contractClientProvider", existingContractClientProvider);
            model.addAttribute("concurrency", false);
            return new ModelAndView("contractclientproviders/edit");
        } else if(contractClientProvider.getVersion() != existingContractClientProvider.getVersion() && "apply".equals(concurrencyControl)){
            // Update the version field to be able to override the existing values
            contractClientProvider.setVersion(existingContractClientProvider.getVersion());
        }
        ContractClientProvider savedContractClientProvider = getContractClientProviderService().save(contractClientProvider);
        UriComponents showURI = getItemLink().to(ContractClientProvidersItemThymeleafLinkFactory.SHOW).with("contractClientProvider", savedContractClientProvider.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProvider
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> ContractClientProvidersItemThymeleafController.delete(@ModelAttribute ContractClientProvider contractClientProvider) {
        getContractClientProviderService().delete(contractClientProvider);
        return ResponseEntity.ok().build();
    }
    
}
