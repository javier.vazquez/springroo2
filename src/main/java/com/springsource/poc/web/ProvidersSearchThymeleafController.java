package com.springsource.poc.web;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.finder.RooSearch;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = ProvidersSearchThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Provider.class, type = ControllerType.SEARCH)
@RooSearch(finders = { "findByDescription" })
@RooThymeleaf
public class ProvidersSearchThymeleafController {
}
