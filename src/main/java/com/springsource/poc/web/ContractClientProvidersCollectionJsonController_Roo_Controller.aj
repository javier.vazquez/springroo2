// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.service.api.ContractClientProviderService;
import com.springsource.poc.web.ContractClientProvidersCollectionJsonController;

privileged aspect ContractClientProvidersCollectionJsonController_Roo_Controller {
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private ContractClientProviderService ContractClientProvidersCollectionJsonController.contractClientProviderService;
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ContractClientProviderService
     */
    public ContractClientProviderService ContractClientProvidersCollectionJsonController.getContractClientProviderService() {
        return contractClientProviderService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProviderService
     */
    public void ContractClientProvidersCollectionJsonController.setContractClientProviderService(ContractClientProviderService contractClientProviderService) {
        this.contractClientProviderService = contractClientProviderService;
    }
    
}
