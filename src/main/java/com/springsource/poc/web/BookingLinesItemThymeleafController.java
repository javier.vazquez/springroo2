package com.springsource.poc.web;
import com.springsource.poc.domain.BookingLine;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = BookingLinesItemThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = BookingLine.class, type = ControllerType.ITEM)
@RooThymeleaf
public class BookingLinesItemThymeleafController {
}
