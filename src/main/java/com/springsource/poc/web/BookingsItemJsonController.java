package com.springsource.poc.web;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = BookingsItemJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Booking.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
public class BookingsItemJsonController {
}
