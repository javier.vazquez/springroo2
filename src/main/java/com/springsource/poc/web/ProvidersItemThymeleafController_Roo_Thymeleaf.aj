// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.domain.Provider;
import com.springsource.poc.service.api.ProviderService;
import com.springsource.poc.web.ProvidersItemThymeleafController;
import com.springsource.poc.web.ProvidersItemThymeleafLinkFactory;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

privileged aspect ProvidersItemThymeleafController_Roo_Thymeleaf {
    
    declare @type: ProvidersItemThymeleafController: @Controller;
    
    declare @type: ProvidersItemThymeleafController: @RequestMapping(value = "/providers/{provider}", name = "ProvidersItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE);
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private MessageSource ProvidersItemThymeleafController.messageSource;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private MethodLinkBuilderFactory<ProvidersItemThymeleafController> ProvidersItemThymeleafController.itemLink;
    
    /**
     * TODO Auto-generated constructor documentation
     * 
     * @param providerService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public ProvidersItemThymeleafController.new(ProviderService providerService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setProviderService(providerService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(ProvidersItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     * 
     * @return MessageSource
     */
    public MessageSource ProvidersItemThymeleafController.getMessageSource() {
        return messageSource;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param messageSource
     */
    public void ProvidersItemThymeleafController.setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<ProvidersItemThymeleafController> ProvidersItemThymeleafController.getItemLink() {
        return itemLink;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param itemLink
     */
    public void ProvidersItemThymeleafController.setItemLink(MethodLinkBuilderFactory<ProvidersItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param id
     * @param locale
     * @param method
     * @return Provider
     */
    @ModelAttribute
    public Provider ProvidersItemThymeleafController.getProvider(@PathVariable("provider") Long id, Locale locale, HttpMethod method) {
        Provider provider = null;
        if (HttpMethod.PUT.equals(method)) {
            provider = providerService.findOneForUpdate(id);
        } else {
            provider = providerService.findOne(id);
        }
        
        if (provider == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] {"Provider", id}, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return provider;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param provider
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView ProvidersItemThymeleafController.show(@ModelAttribute Provider provider, Model model) {
        model.addAttribute("provider", provider);
        return new ModelAndView("providers/show");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param provider
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView ProvidersItemThymeleafController.showInline(@ModelAttribute Provider provider, Model model) {
        model.addAttribute("provider", provider);
        return new ModelAndView("providers/showInline :: inline-content");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param dataBinder
     */
    @InitBinder("provider")
    public void ProvidersItemThymeleafController.initProviderBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param model
     */
    public void ProvidersItemThymeleafController.populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param model
     */
    public void ProvidersItemThymeleafController.populateForm(Model model) {
        populateFormats(model);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param provider
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView ProvidersItemThymeleafController.editForm(@ModelAttribute Provider provider, Model model) {
        populateForm(model);
        
        model.addAttribute("provider", provider);
        return new ModelAndView("providers/edit");
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param provider
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView ProvidersItemThymeleafController.update(@Valid @ModelAttribute Provider provider, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            
            return new ModelAndView("providers/edit");
        }
        // Concurrency control
        Provider existingProvider = getProviderService().findOne(provider.getId());
        if(provider.getVersion() != existingProvider.getVersion() && StringUtils.isEmpty(concurrencyControl)){
            populateForm(model);
            model.addAttribute("provider", provider);
            model.addAttribute("concurrency", true);
            return new ModelAndView("providers/edit");
        } else if(provider.getVersion() != existingProvider.getVersion() && "discard".equals(concurrencyControl)){
            populateForm(model);
            model.addAttribute("provider", existingProvider);
            model.addAttribute("concurrency", false);
            return new ModelAndView("providers/edit");
        } else if(provider.getVersion() != existingProvider.getVersion() && "apply".equals(concurrencyControl)){
            // Update the version field to be able to override the existing values
            provider.setVersion(existingProvider.getVersion());
        }
        Provider savedProvider = getProviderService().save(provider);
        UriComponents showURI = getItemLink().to(ProvidersItemThymeleafLinkFactory.SHOW).with("provider", savedProvider.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param provider
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> ProvidersItemThymeleafController.delete(@ModelAttribute Provider provider) {
        getProviderService().delete(provider);
        return ResponseEntity.ok().build();
    }
    
}
