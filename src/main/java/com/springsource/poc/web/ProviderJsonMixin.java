package com.springsource.poc.web;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;

/**
 * = ProviderJsonMixin
 TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Provider.class)
public abstract class ProviderJsonMixin {
}
