package com.springsource.poc.web;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = BookingsCollectionThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Booking.class, type = ControllerType.COLLECTION)
@RooThymeleaf
public class BookingsCollectionThymeleafController {
}
