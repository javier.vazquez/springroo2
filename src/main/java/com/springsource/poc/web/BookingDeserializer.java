package com.springsource.poc.web;
import com.springsource.poc.domain.Booking;
import com.springsource.poc.service.api.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;

/**
 * = BookingDeserializer
 TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Booking.class)
public class BookingDeserializer extends JsonObjectDeserializer<Booking> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private BookingService bookingService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param bookingService
     * @param conversionService
     */
    @Autowired
    public BookingDeserializer(@Lazy BookingService bookingService, ConversionService conversionService) {
        this.bookingService = bookingService;
        this.conversionService = conversionService;
    }
}
