package com.springsource.poc.web;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = BookingsItemThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Booking.class, type = ControllerType.ITEM)
@RooThymeleaf
public class BookingsItemThymeleafController {
}
