package com.springsource.poc.web;
import com.springsource.poc.domain.ContractClientProvider;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = ContractClientProvidersCollectionJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = ContractClientProvider.class, pathPrefix = "/api", type = ControllerType.COLLECTION)
@RooJSON
public class ContractClientProvidersCollectionJsonController {
}
