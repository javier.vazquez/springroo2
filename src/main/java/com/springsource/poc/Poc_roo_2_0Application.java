package com.springsource.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * = Poc_roo_2_0Application
 *
 * TODO Auto-generated class documentation
 *
 */
@SpringBootApplication
public class Poc_roo_2_0Application {

    /**
     * TODO Auto-generated method documentation
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Poc_roo_2_0Application.class, args);
    }
}