// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.domain.dod.ProviderFactory;
import com.springsource.poc.service.api.BookingLineService;
import com.springsource.poc.service.api.ContractClientProviderService;
import com.springsource.poc.service.api.ProviderService;
import com.springsource.poc.web.ProvidersSearchThymeleafController;
import com.springsource.poc.web.ProvidersSearchThymeleafControllerIT;
import io.springlets.boot.test.autoconfigure.web.servlet.SpringletsWebMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

privileged aspect ProvidersSearchThymeleafControllerIT_Roo_ThymeleafControllerIntegrationTest {
    
    declare @type: ProvidersSearchThymeleafControllerIT: @RunWith(SpringRunner.class);
    
    declare @type: ProvidersSearchThymeleafControllerIT: @SpringletsWebMvcTest(controllers = ProvidersSearchThymeleafController.class, secure = false);
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @Autowired
    private MockMvc ProvidersSearchThymeleafControllerIT.mvc;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @MockBean
    private ProviderService ProvidersSearchThymeleafControllerIT.providerServiceService;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @MockBean
    private BookingLineService ProvidersSearchThymeleafControllerIT.bookingLineServiceService;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @MockBean
    private ContractClientProviderService ProvidersSearchThymeleafControllerIT.contractClientProviderServiceService;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private ProviderFactory ProvidersSearchThymeleafControllerIT.factory = new ProviderFactory();
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return MockMvc
     */
    public MockMvc ProvidersSearchThymeleafControllerIT.getMvc() {
        return mvc;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ProviderService
     */
    public ProviderService ProvidersSearchThymeleafControllerIT.getProviderServiceService() {
        return providerServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param providerServiceService
     */
    public void ProvidersSearchThymeleafControllerIT.setProviderServiceService(ProviderService providerServiceService) {
        this.providerServiceService = providerServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return BookingLineService
     */
    public BookingLineService ProvidersSearchThymeleafControllerIT.getBookingLineServiceService() {
        return bookingLineServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookingLineServiceService
     */
    public void ProvidersSearchThymeleafControllerIT.setBookingLineServiceService(BookingLineService bookingLineServiceService) {
        this.bookingLineServiceService = bookingLineServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ContractClientProviderService
     */
    public ContractClientProviderService ProvidersSearchThymeleafControllerIT.getContractClientProviderServiceService() {
        return contractClientProviderServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProviderServiceService
     */
    public void ProvidersSearchThymeleafControllerIT.setContractClientProviderServiceService(ContractClientProviderService contractClientProviderServiceService) {
        this.contractClientProviderServiceService = contractClientProviderServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ProviderFactory
     */
    public ProviderFactory ProvidersSearchThymeleafControllerIT.getFactory() {
        return factory;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param factory
     */
    public void ProvidersSearchThymeleafControllerIT.setFactory(ProviderFactory factory) {
        this.factory = factory;
    }
    
    /**
     * Test method example. To be implemented by developer.
     * 
     */
    @Test
    public void ProvidersSearchThymeleafControllerIT.testMethodExample() {
        // Setup
        // Previous tasks
        
        // Exercise
        // Execute method to test
        
        // Verify
        // Check results with assertions
    }
    
}
