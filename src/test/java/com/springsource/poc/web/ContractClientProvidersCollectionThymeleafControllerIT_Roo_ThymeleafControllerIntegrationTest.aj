// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.web;

import com.springsource.poc.domain.dod.ContractClientProviderFactory;
import com.springsource.poc.service.api.BookingLineService;
import com.springsource.poc.service.api.ContractClientProviderService;
import com.springsource.poc.web.ContractClientProvidersCollectionThymeleafController;
import com.springsource.poc.web.ContractClientProvidersCollectionThymeleafControllerIT;
import io.springlets.boot.test.autoconfigure.web.servlet.SpringletsWebMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

privileged aspect ContractClientProvidersCollectionThymeleafControllerIT_Roo_ThymeleafControllerIntegrationTest {
    
    declare @type: ContractClientProvidersCollectionThymeleafControllerIT: @RunWith(SpringRunner.class);
    
    declare @type: ContractClientProvidersCollectionThymeleafControllerIT: @SpringletsWebMvcTest(controllers = ContractClientProvidersCollectionThymeleafController.class, secure = false);
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @Autowired
    private MockMvc ContractClientProvidersCollectionThymeleafControllerIT.mvc;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @MockBean
    private ContractClientProviderService ContractClientProvidersCollectionThymeleafControllerIT.contractClientProviderServiceService;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @MockBean
    private BookingLineService ContractClientProvidersCollectionThymeleafControllerIT.bookingLineServiceService;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private ContractClientProviderFactory ContractClientProvidersCollectionThymeleafControllerIT.factory = new ContractClientProviderFactory();
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return MockMvc
     */
    public MockMvc ContractClientProvidersCollectionThymeleafControllerIT.getMvc() {
        return mvc;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ContractClientProviderService
     */
    public ContractClientProviderService ContractClientProvidersCollectionThymeleafControllerIT.getContractClientProviderServiceService() {
        return contractClientProviderServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param contractClientProviderServiceService
     */
    public void ContractClientProvidersCollectionThymeleafControllerIT.setContractClientProviderServiceService(ContractClientProviderService contractClientProviderServiceService) {
        this.contractClientProviderServiceService = contractClientProviderServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return BookingLineService
     */
    public BookingLineService ContractClientProvidersCollectionThymeleafControllerIT.getBookingLineServiceService() {
        return bookingLineServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param bookingLineServiceService
     */
    public void ContractClientProvidersCollectionThymeleafControllerIT.setBookingLineServiceService(BookingLineService bookingLineServiceService) {
        this.bookingLineServiceService = bookingLineServiceService;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ContractClientProviderFactory
     */
    public ContractClientProviderFactory ContractClientProvidersCollectionThymeleafControllerIT.getFactory() {
        return factory;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param factory
     */
    public void ContractClientProvidersCollectionThymeleafControllerIT.setFactory(ContractClientProviderFactory factory) {
        this.factory = factory;
    }
    
    /**
     * Test method example. To be implemented by developer.
     * 
     */
    @Test
    public void ContractClientProvidersCollectionThymeleafControllerIT.testMethodExample() {
        // Setup
        // Previous tasks
        
        // Exercise
        // Execute method to test
        
        // Verify
        // Check results with assertions
    }
    
}
