package com.springsource.poc.domain.dod;
import com.springsource.poc.domain.BookingLine;
import org.springframework.roo.addon.jpa.annotations.dod.RooJpaDataOnDemand;

/**
 * = BookingLineDataOnDemand
 TODO Auto-generated class documentation
 *
 */
@RooJpaDataOnDemand(entity = BookingLine.class)
public class BookingLineDataOnDemand {
}
