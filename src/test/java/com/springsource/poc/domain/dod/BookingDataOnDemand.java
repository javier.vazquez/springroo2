package com.springsource.poc.domain.dod;
import com.springsource.poc.domain.Booking;
import org.springframework.roo.addon.jpa.annotations.dod.RooJpaDataOnDemand;

/**
 * = BookingDataOnDemand
 TODO Auto-generated class documentation
 *
 */
@RooJpaDataOnDemand(entity = Booking.class)
public class BookingDataOnDemand {
}
