package com.springsource.poc.domain.dod;
import com.springsource.poc.domain.ContractClientProvider;
import org.springframework.roo.addon.jpa.annotations.dod.RooJpaDataOnDemand;

/**
 * = ContractClientProviderDataOnDemand
 TODO Auto-generated class documentation
 *
 */
@RooJpaDataOnDemand(entity = ContractClientProvider.class)
public class ContractClientProviderDataOnDemand {
}
