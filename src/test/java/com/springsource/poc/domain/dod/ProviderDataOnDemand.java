package com.springsource.poc.domain.dod;
import com.springsource.poc.domain.Provider;
import org.springframework.roo.addon.jpa.annotations.dod.RooJpaDataOnDemand;

/**
 * = ProviderDataOnDemand
 TODO Auto-generated class documentation
 *
 */
@RooJpaDataOnDemand(entity = Provider.class)
public class ProviderDataOnDemand {
}
