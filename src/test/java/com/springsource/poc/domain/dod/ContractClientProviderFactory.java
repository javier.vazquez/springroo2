package com.springsource.poc.domain.dod;
import com.springsource.poc.domain.ContractClientProvider;
import org.springframework.roo.addon.jpa.annotations.entity.factory.RooJpaEntityFactory;

/**
 * = ContractClientProviderFactory
 TODO Auto-generated class documentation
 *
 */
@RooJpaEntityFactory(entity = ContractClientProvider.class)
public class ContractClientProviderFactory {
}
