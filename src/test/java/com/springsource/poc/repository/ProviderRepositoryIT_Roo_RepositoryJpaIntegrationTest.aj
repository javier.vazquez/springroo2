// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.poc.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.springsource.poc.config.SpringDataJpaDetachableRepositoryConfiguration;
import com.springsource.poc.dod.DataOnDemandConfiguration;
import com.springsource.poc.domain.Provider;
import com.springsource.poc.domain.dod.ProviderDataOnDemand;
import com.springsource.poc.repository.ProviderRepository;
import com.springsource.poc.repository.ProviderRepositoryIT;
import io.springlets.data.domain.GlobalSearch;
import java.util.Iterator;
import java.util.List;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

privileged aspect ProviderRepositoryIT_Roo_RepositoryJpaIntegrationTest {
    
    declare @type: ProviderRepositoryIT: @RunWith(SpringRunner.class);
    
    declare @type: ProviderRepositoryIT: @DataJpaTest;
    
    declare @type: ProviderRepositoryIT: @Import({ DataOnDemandConfiguration.class, SpringDataJpaDetachableRepositoryConfiguration.class });
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @Rule
    public ExpectedException ProviderRepositoryIT.thrown = ExpectedException.none();
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @Autowired
    private ProviderRepository ProviderRepositoryIT.repository;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    @Autowired
    private ProviderDataOnDemand ProviderRepositoryIT.dod;
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ProviderRepository
     */
    public ProviderRepository ProviderRepositoryIT.getRepository() {
        return repository;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return ProviderDataOnDemand
     */
    public ProviderDataOnDemand ProviderRepositoryIT.getDod() {
        return dod;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Before
    public void ProviderRepositoryIT.checkDataOnDemandHasInitializedCorrectly() {
        assertThat(getDod().getRandomProvider())
            .as("Check data on demand for 'Provider' initializes correctly by getting a random Provider")
            .isNotNull();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Test
    public void ProviderRepositoryIT.countShouldReturnExpectedValue() {
        // Verify
        assertThat(getRepository().count()).as("Check there are available 'Provider' entries").isGreaterThan(0);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Test
    public void ProviderRepositoryIT.findOneShouldReturnExistingProvider() {
        // Setup
        Long id = getRandomProviderId();
        
        // Exercise
        Provider provider = getRepository().findOne(id);
        
        // Verify
        assertThat(provider).as("Check that findOne illegally returned null for id %s", id).isNotNull();
        assertThat(id).as("Check the identifier of the found 'Provider' is the same used to look for it")
            .isEqualTo(provider.getId());
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Test
    public void ProviderRepositoryIT.findAllShouldReturnAllProviders() {
        // Setup
        assertThat(getRepository().count())
            .as("Check the number of entries is not too big (250 entries). "
                + "If it is, please review the tests so it doesn't take too long to run them")
            .isLessThan(250);
        
        // Exercise
        List<Provider> result = getRepository().findAll();
        
        // Verify
        assertThat(result).as("Check 'findAll' returns a not null list of entries").isNotNull();
        assertThat(result.size()).as("Check 'findAll' returns a not empty list of entries")
            .isGreaterThan(0);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Test
    public void ProviderRepositoryIT.persistShouldGenerateIdValue() {
        // Setup
        // Exercise
        Provider provider = getDod().getNewRandomTransientProvider();
        
        // Verify
        assertThat(provider).as("Check the Data on demand generated a new non null 'Provider'").isNotNull();
        assertThat(provider.getId()).as("Check the Data on demand generated a new 'Provider' whose id is null")
            .isNull();
        try {
            provider = getRepository().saveAndFlush(provider);
        } catch (final ConstraintViolationException e) {
            final StringBuilder msg = new StringBuilder();
            for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter
                .hasNext();) {
                final ConstraintViolation<?> cv = iter.next();
                msg.append("[").append(cv.getRootBean().getClass().getName()).append(".")
                    .append(cv.getPropertyPath()).append(": ").append(cv.getMessage())
                    .append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
            }
            throw new IllegalStateException(msg.toString(), e);
        }
        assertThat(provider.getId()).as("Check a 'Provider' (%s) id is not null after been persisted", provider)
            .isNotNull();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Test
    public void ProviderRepositoryIT.deleteShouldMakeProviderUnavailable() {
        // Setup
        Long id = getRandomProviderId();
        Provider provider = getRepository().findOne(id);
        
        // Exercise
        getRepository().delete(provider);
        
        // Verify
        assertThat(getRepository().findOne(id))
        .as("Check the deleted 'Provider' %s is no longer available with 'findOne'", provider).isNull();
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Test
    public void ProviderRepositoryIT.findAllCustomNotFilteredNotPagedShouldReturnAllProviders() {
        // Exercise
        Page<Provider> all = getRepository().findAll((GlobalSearch) null, new PageRequest(0, getDod().getSize()));
        
        // Verify
        assertThat(all.getNumberOfElements())
            .as("Check 'findAll' with null 'GlobalSearch' and no pagination returns all entries")
            .isEqualTo(getDod().getSize());
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     */
    @Test
    public void ProviderRepositoryIT.findAllCustomNotFilteredPagedShouldReturnAProvidersPage() {
        // Exercise
        Page<Provider> all = getRepository().findAll((GlobalSearch) null, new PageRequest(0, 3));
        
        // Verify
        assertThat(all.getNumberOfElements())
            .as("Check result number is not greater than the page size").isLessThanOrEqualTo(3);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return Long
     */
    private Long ProviderRepositoryIT.getRandomProviderId() {
        Provider provider = getDod().getRandomProvider();
        Long id = provider.getId();
        assertThat(id).as("Check the Data on demand generated a 'Provider' with an identifier").isNotNull();
        return id;
    }
    
}
