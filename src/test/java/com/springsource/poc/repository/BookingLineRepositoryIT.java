package com.springsource.poc.repository;
import com.springsource.poc.dod.DataOnDemandConfiguration;
import com.springsource.poc.domain.dod.BookingLineDataOnDemand;
import org.springframework.roo.addon.layers.repository.jpa.annotations.test.RooRepositoryJpaIntegrationTest;

/**
 * = BookingLineRepositoryIT
 TODO Auto-generated class documentation
 *
 */
@RooRepositoryJpaIntegrationTest(targetClass = BookingLineRepository.class, dodConfigurationClass = DataOnDemandConfiguration.class, dodClass = BookingLineDataOnDemand.class)
public class BookingLineRepositoryIT {
}
