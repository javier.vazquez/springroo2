package com.springsource.poc.repository;
import com.springsource.poc.dod.DataOnDemandConfiguration;
import com.springsource.poc.domain.dod.ContractClientProviderDataOnDemand;
import org.springframework.roo.addon.layers.repository.jpa.annotations.test.RooRepositoryJpaIntegrationTest;

/**
 * = ContractClientProviderRepositoryIT
 TODO Auto-generated class documentation
 *
 */
@RooRepositoryJpaIntegrationTest(targetClass = ContractClientProviderRepository.class, dodConfigurationClass = DataOnDemandConfiguration.class, dodClass = ContractClientProviderDataOnDemand.class)
public class ContractClientProviderRepositoryIT {
}
