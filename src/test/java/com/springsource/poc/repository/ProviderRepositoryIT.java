package com.springsource.poc.repository;
import com.springsource.poc.dod.DataOnDemandConfiguration;
import com.springsource.poc.domain.dod.ProviderDataOnDemand;
import org.springframework.roo.addon.layers.repository.jpa.annotations.test.RooRepositoryJpaIntegrationTest;

/**
 * = ProviderRepositoryIT
 TODO Auto-generated class documentation
 *
 */
@RooRepositoryJpaIntegrationTest(targetClass = ProviderRepository.class, dodConfigurationClass = DataOnDemandConfiguration.class, dodClass = ProviderDataOnDemand.class)
public class ProviderRepositoryIT {
}
