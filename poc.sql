CREATE DATABASE IF NOT EXISTS `poc` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `poc`;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla poc.provider
CREATE TABLE IF NOT EXISTS `provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla poc.contract_client_provider
CREATE TABLE IF NOT EXISTS `contract_client_provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla poc.contract_client_provider_providers
CREATE TABLE IF NOT EXISTS `contract_client_provider_providers` (
  `contract_client_providers_id` bigint(20) NOT NULL,
  `providers_id` bigint(20) NOT NULL,
  PRIMARY KEY (`contract_client_providers_id`,`providers_id`),
  KEY `FKheex7gegx5kw6i56dh9nb5fh9` (`providers_id`),
  CONSTRAINT `FKeugoh2dia8h4u6gp8tama79bl` FOREIGN KEY (`contract_client_providers_id`) REFERENCES `contract_client_provider` (`id`),
  CONSTRAINT `FKheex7gegx5kw6i56dh9nb5fh9` FOREIGN KEY (`providers_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando estructura para tabla poc.booking_line
CREATE TABLE IF NOT EXISTS `booking_line` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cancelled` bit(1) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `from_date` datetime NOT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `price_cost` decimal(19,2) NOT NULL,
  `price_sell` decimal(19,2) NOT NULL,
  `status` int(11) NOT NULL,
  `to_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `contract_client_provider_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcbyvvd5j3cdaf8reg0m563jq3` (`contract_client_provider_id`),
  CONSTRAINT `FKcbyvvd5j3cdaf8reg0m563jq3` FOREIGN KEY (`contract_client_provider_id`) REFERENCES `contract_client_provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando estructura para tabla poc.booking_line
CREATE TABLE IF NOT EXISTS `booking` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla poc.booking_line_providers
CREATE TABLE IF NOT EXISTS `booking_line_providers` (
  `booking_lines_id` bigint(20) NOT NULL,
  `providers_id` bigint(20) NOT NULL,
  PRIMARY KEY (`booking_lines_id`,`providers_id`),
  KEY `FKrvo8sxpkx6bk9wwdf7ij7taqw` (`providers_id`),
  CONSTRAINT `FKffndk6xevgxaaqqooj59aumae` FOREIGN KEY (`booking_lines_id`) REFERENCES `booking_line` (`id`),
  CONSTRAINT `FKrvo8sxpkx6bk9wwdf7ij7taqw` FOREIGN KEY (`providers_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
